<?php
// Include config file
require_once "config.php";

$type = "";
$type_err= "";

$red="";

// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

if(isset($_POST["type"])) { 
        $param_app = $_POST["type"];
		
		if($param_app == 'Refrigerator')
		{
            echo "Refrigerator";
            $red = 1;
		}
		else if($param_app == 'Washing')
		{
            echo "Washing Machine";     
            $red = 2;
		}
		else if($param_app == 'ac')
		{
            echo "AC";
            $red = 3;
		}
		else if($param_app == 'MW')
		{
            echo "MicroWave";
            $red = 4;
            }
		else if($param_app == 'TV')
		{
            echo "LCD/LED";
            $red = 5;
		}
    }

if($_SERVER["REQUEST_METHOD"] == "POST")
{

$sql = "UPDATE users SET app = ? WHERE id = ?";

  if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_app, $param_id);
            
            // Set parameters
           
            $param_id = $_SESSION["id"];
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
				echo "  Appliance added is $param_app";
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
	 mysqli_close($link);	
}

if($red==1)
header('Location: brand_refrigerator.php');
else if($red==2)
header('Location: brand_washing.php');
else if($red==3)
header('Location: brand_ac.php');
else if($red==4)
header('Location: brand_mw.php');
else if($red==5)
header('Location: brand_tv.php');

?>