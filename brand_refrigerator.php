<?php
// Include config file
require_once "config.php";

$type = "";
$type_err= "";

// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

if(isset($_POST["type"])) { 
        $param_app = $_POST["type"];
        echo "You have chosen $param_app";
    }

if($_SERVER["REQUEST_METHOD"] == "POST")
{

$sql = "UPDATE users SET brand = ? WHERE id = ?";

  if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_app, $param_id);
            
            // Set parameters
           
            $param_id = $_SESSION["id"];
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
				echo "  Appliance added is $param_app";
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
	 mysqli_close($link);	
}

?>

<!DOCTYPE html>
<html lang="en">
<head>    <meta charset="UTF-8">
    <head><meta charset="utf-8">
	<meta name="viewport" content="width=device-width, maximum-scale=1">

	<title>Buy-Insurance</title>
	<link rel="icon" href="favicon.png" type="image/png">
	<link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='css/form.css' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">

</head>
<body>


<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">INSTAGADGET</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">

      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo htmlspecialchars($_SESSION["name"]); ?> <span class="glyphicon glyphicon-user"></span></a>
      <ul class="dropdown-menu">
          <li><a href="reset-password.php">Reset Password</a></li>
        </ul>
    </li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>


		 <div class="wrapper">
        <p>FILL THE DETAILS BELOW</p>
	
	

        <form action="thanks.php" method="post">
Select Brand
<br>
  <select class="form-control" name="brand">
    <option value="samsung">Samsung</option>
    <option value="lg" >LG</option>
    <option value="goodrej">Godrej</option>
    <option value="volt">Voltas</option>
	<option value="philip">Philips</option>
  <option value="OTR">Other</option>
  </select>
  <br><br>


  Specify if Other Brand ?
<input class="form-control" type="text" name="other">
  <br><br>
  <input type="submit" style="
    position: absolute;
    bottom:   0;
    right: 0;
">

Select Type
<br>
  <select class="form-control" name="type">
    <option value="sngl">Single Door</option>
    <option value="dbl" >Double Door</option>
    <option value="trpl">Triple Door</option>
    <option value="sbs">Side by Side</option>
  </select>
  <br><br>

Select Size in LTR.
<br>
  <select class="form-control" name="size">
    <option value="1">0-100 LTR</option>
    <option value="2" >100-200 LTR</option>
    <option value="3">200-300 LTR</option>
    <option value="4">300-400 LTR</option>
    <option value="5">ABOVE 400 LTR</option>
  </select>
  <br><br>
  Is it in warranty ? <br>
  <input type="radio" name="warr"
<?php if (isset($type) && $type=="1") echo "checked";?>
value="1">Yes
<input type="radio" name="warr"
<?php if (isset($type) && $type=="0") echo "checked";?>
value="0">No
<br><br>
Year of purchase

<input  type="text" name="year" class="md-textarea form-control">

<br><br>
  <input type="submit" style="
    position: absolute;
    bottom:   0;
    right: 0;
">
</form>	   
	
	   
    </div>    
		
    </p>

    
</body>
</html>