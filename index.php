<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, maximum-scale=1">

	<title>Homepage</title>
	<link rel="icon" href="favicon.png" type="image/png">
	<link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/jquery.isotope.js"></script>
	<script type="text/javascript" src="js/wow.js"></script>
	<script type="text/javascript" src="js/classie.js"></script>
	<script type="text/javascript" src="js/magnific-popup.js"></script>
	<script src="contactform/contactform.js"></script>

</head>

<body>
	<header class="header" id="header">
		<!--header-start-->
		<div class="container">
			<figure class="logo animated fadeInDown delay-07s">
				<a href="#"><img src="img/logo.png" alt=""></a>
			</figure>
			<h1 class="animated fadeInDown delay-07s">Welcome To Instagadget Secure</h1>
			<ul class="we-create animated fadeInUp delay-1s">
				<li>Secure all your appliances.</li>
			</ul>
			<a class="link animated fadeInUp delay-1s servicelink" href="register.php" style="
    border-radius: 200px;
">Register New Product</a>
		</div>
	</header>
	<!--header-end-->

	<nav class="main-nav-outer" id="test">
		<!--main-nav-start-->
		<div class="container">
			<ul class="main-nav">
				<li><a href="#header">Home</a></li>
				<li><a href="#service">Services</a></li>
				<li class="small-logo"><a href="#header"><img src="img/small-logo.png" alt=""></a></li>
				<li><a href="#contact">Contact</a></li>
				<li><a href="login.php">Login</a></li>
			</ul>
			<a class="res-nav_click" href="#"><i class="fa fa-bars"></i></a>
		</div>
	</nav>
	<!--main-nav-end-->



	<section class="main-section" id="service">
		<!--main-section-start-->
		<div class="container">
			<h2>Services</h2>
			<h6>We offer exceptional service with complimentary hugs.</h6>
			<div class="row">
				<div class="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
					<div class="service-list">
						<div class="service-list-col1">
							<i class="fa fa-paw"></i>
						</div>
						<a href="#1" class="service-list-col2">
							<h3>Door Step Service</h3>
						</a>
					</div>
					<div class="service-list">
						<div class="service-list-col1">
							<i class="fa fa-gear"></i>
						</div>
						<a href="#2" class="service-list-col2">
							<h3>ANNUAL MAINTENANCE CONTRACT</h3>
						</a>
					</div>
					<div class="service-list">
						<div class="service-list-col1">
							<i class="fa fa-apple"></i>
						</div>
						<a href="#3" class="service-list-col2">
							<h3>Genuine Spare Parts</h3>
						</a>
					</div>
									</div>
				<figure class="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
					<img src="img/macbook-pro.jpg" alt="">
				</figure>
 
			</div>
		</div>
	</section>
	<!--main-section-end-->



	<section class="main-section alabaster">
		<!--main-section alabaster-start-->
		<div class="container">
			<div class="row">
				<figure class="col-lg-5 col-sm-4 wow fadeInLeft">
					<img src="img/iphone.png" alt="">
				</figure>
				<div class="col-lg-7 col-sm-8 featured-work">
					<h2>featured work</h2>
					<P class="padding-b">Insta Gadget Secure Service covers repair service (labour) of all your home appliances Insta Gadget Secure Repair Service does not cover mobile phones, tablets, gaming consoles, iPods and other MP3 players, cameras and Apple products.</P>
					
				</div>
			</div>
		</div>
	</section>
	<!--main-section alabaster-end-->

<section style="padding:10%">
<div class="featured-work" style="width: auto">
<h2>
	What Do You Know About Us
</h2>
<h1>
WHO WE ARE ?
</h1>
<p style="text-align:justify">
An average Indian household today uses at least 10 to 15 electrical appliances and electronic devices of varying age and brands & everyday each individual faces such small or big problems. The main motive behind Insta Gadget Secure  is to address these very concerns of consumers and act as a single window solutions provider in regard to customer’s decision on the choice of technology, size, installation, maintenance, repair,and , Electrical Appliances,  and all other possible Home Appliances. Our technicians are well trained and undergo frequent skill up-gradation programs to keep up with the technological advancements.
Innovative services, after-service solutions, call center facility, web consultancy services, assured safety and thorough market research to incorporate new servicing techniques are our strength which have made us mark our presence all over within a short span.

</p>
<section id="1">
<h2>
DOOR STEP SERVICE
</h2>
</section>
<p style="text-align:justify">
	Door step Service:
An average Indian household today uses at least 10 to 15 electrical appliances and electronic devices of varying age and brands which demand proper installation, calibration, proper accessories and continuous tuning to give optimum performance on a sustained basis for a minimum period of 8 to 10 years.
With high paced advancement of technology, electrical products and appliances face quick obsolescence making the owners search for spare parts for months without any gain, at times
</p>

<h2 id="2">
AMC
</h2>
<h1>
About
</h1>
<p style="text-align:justify">
ANNUAL MAINTENANCE CONTRACT (AMC)
Electronic devices no doubt make our life easy and convenient. But their unexpected breakdown cannot be ruled out.
It is hence important to have a regular periodic care and check up to ensure smooth functioning of your trust your electronic devices and appliances.
Insta Gadget Secure AMC gets you full year coverage -service - for your covered home electronic/appliance Preventive maintenance visit during the year Unlimited breakdown coverage - at home or on phone.
</p>

<h1>
Why AMC with Insta Gadget Secure:
</h1>
<p style="text-align:justify">
The answer is Insta Gadget Secure gives the best benefits out of AMC coverage that prevents your expensive electrical and electronic devices from temporary or permanent breakdowns.
As is said ‘ prevention is better than cure’ , with due periodic maintenance, these devices can be protected and the high costs of repair avoided, allowing you peace of mind with regards to their usage.
</p>

<h1>
Benefits of AMC:
</h1>
<p style="text-align:justify">

•	A one-time fee for year-long assurance with your everyday devices like Television, Refrigerator, Washing Machine, Air-conditioners, and Micro-wave
<br>
•	Flexibility to design your own protection service plan.
<br>
•	Also relocation support when you move to a different place/city.
<br>
•	Warranty on workmanship
<br>
•	We use only genuine and branded spares.
<br>
•	We stand behind our work and offer a 30-day service guarantee on all workmanship
<br>
•	Preventive maintenance visit during the year
<br>
•	Unlimited breakdown coverage - at home or on phone
</p>

<h1>
Convenience factor during sign up and requesting service:
</h1>
<p style="text-align:justify">

•	Flexibility to either choose one of our recommended plans or design your own protection plan and include as many devices in your household to ensure complete peace of mind
<br>
•	Sign up from the convenience of your home, through our technician, through phone or even online
<br>
•	Call anytime for help with your devices or any related advise

</p>

<h2 id="3">
	Genuine Spare Parts:
</h2>
<p style="text-align:justify">
Many people believe in saving a few rupees by buying/using duplicate parts which are “as good as the original”. It is always advisable to use Genuine and Branded Spare parts which have been tested for reliability. This will allow you to enjoy optimum product performance and will not compromise other critical parts of the product.
Cost of a repeat failure is usually more than the saving from a duplicate part.
</p>



</div>


<button id="faq" onClick="Faq()" style="
    width: auto;
	min-width: 150px;
    border: none;
    border-radius: 50px;
    background: red;
    color: white;
    font-weight: bold;
    letter-spacing: 2px;
    height: 40px;
    font-size: 22px;
	margin: 22px;">SHOW FAQ</button>

<div id="faq_content" style="display: none;" class="animated bounceInDown">
<h2>
FAQ
</h2>
<h3>5.1 What kind of repairs does Insta Gadget Secure Repair Service cover?
</h3>Ans: Insta Gadget Secure Service covers repair service (labour) of all your home appliances Insta Gadget Secure Repair Service does not cover mobile phones, tablets, gaming consoles, iPods and other MP3 players, cameras and Apple products.
<br>
<br>
<h3>5.2 Why should I go for Insta Gadget Secure Maintenance Plan?
</h3>Ans: A household consists multiple electrical and electronic devices and appliances. Over time, they tend to wear out. With due periodic maintenance, these devices can be protected and the high costs of repair avoided, allowing you peace of mind with regards to their usage.
<br>
<br>
<h3>5.3	Why should I go for a Insta Gadget Secure Repair Service? 
</h3>Ans:
•	Insta Gadget secure  is a one point contact for all appliance service needs
•	  We only use genuine spare parts
<br>
<br>
<h3>5.4	How do I sign up for Insta Gadget Secure Maintenance Plans online?
</h3>Ans :
•	Contact Us or Call us on our Number (+91) 7785878587 
OR
•	  Email us at enquiries@Instagadgetsecure.in
<br>
<br>
<h3>5.5	How quickly will my request be processed?
</h3>Ans : If you place request between 10am to 8pm (IST) you will be responded with a phone call from our IGS care executive within 45 minutes. Any time before or after the mentioned times, you will receive a call from us the next working day. On phone confirmation, the request will be processed immediately.

</div>


</section>

	<section class="business-talking">
		<!--business-talking-start-->
		<div class="container">
			<h2>Secure your gadgets and appliances.</h2>
		</div>
	</section>
	<!--business-talking-end-->

	<div class="container">
		<section class="main-section contact" id="contact">

			<div class="row">
				<div class="col-lg-6 col-sm-7 wow fadeInLeft">
					<div class="contact-info-box address clearfix">
						<h3><i class=" icon-map-marker"></i>Address:</h3>
						<span>308 Negra Arroyo Lane<br>Albuquerque, New Mexico, 87111.</span>
					</div>
					<div class="contact-info-box phone clearfix">
						<h3><i class="fa fa-phone"></i>Phone:</h3>
						<span>1-800-BOO-YAHH</span>
					</div>
					<div class="contact-info-box email clearfix">
						<h3><i class="fa fa-pencil"></i>email:</h3>
						<span>hello@knightstudios.com</span>
					</div>
					<div class="contact-info-box hours clearfix">
						<h3><i class="fa fa-clock-o"></i>Hours:</h3>
						<span><strong>Monday - Thursday:</strong> 10am - 6pm<br><strong>Friday:</strong> People work on Fridays now?<br><strong>Saturday - Sunday:</strong> Best not to ask.</span>
					</div>
					<ul class="social-link">
						<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
						<li class="gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li class="dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
					</ul>
				</div>
		
			</div>
		</section>
	</div>
	<footer class="footer">
		<div class="container">
			<div class="footer-logo"><a href="#"><img src="img/footer-logo.png" alt=""></a></div>
			<div class="credits">
			
				Made by <a href="https://imkartikid.github.io/x-bit/">x-bit developers</a>
			</div>
		</div>
	</footer>


	<script type="text/javascript">
		$(document).ready(function(e) {

			$('#test').scrollToFixed();
			$('.res-nav_click').click(function() {
				$('.main-nav').slideToggle();
				return false

			});

      $('.Portfolio-box').magnificPopup({
        delegate: 'a',
        type: 'image'
      });

		});
	</script>

	<script>
		wow = new WOW({
			animateClass: 'animated',
			offset: 100
		});
		wow.init();
	</script>

<script src="js/app.js">
</script>

	<script type="text/javascript">
		$(window).load(function() {

			$('.main-nav li a, .servicelink').bind('click', function(event) {
				var $anchor = $(this);

				$('html, body').stop().animate({
					scrollTop: $($anchor.attr('href')).offset().top - 102
				}, 1500, 'easeInOutExpo');
				/*
				if you don't want to use the easing effects:
				$('html, body').stop().animate({
					scrollTop: $($anchor.attr('href')).offset().top
				}, 1000);
				*/
				if ($(window).width() < 768) {
					$('.main-nav').hide();
				}
				event.preventDefault();
			});
		})
	</script>

	<script type="text/javascript">
		$(window).load(function() {


			var $container = $('.portfolioContainer'),
				$body = $('body'),
				colW = 375,
				columns = null;


			$container.isotope({
				// disable window resizing
				resizable: true,
				masonry: {
					columnWidth: colW
				}
			});

			$(window).smartresize(function() {
				// check if columns has changed
				var currentColumns = Math.floor(($body.width() - 30) / colW);
				if (currentColumns !== columns) {
					// set new column count
					columns = currentColumns;
					// apply width to container manually, then trigger relayout
					$container.width(columns * colW)
						.isotope('reLayout');
				}

			}).smartresize(); // trigger resize to set container width
			$('.portfolioFilter a').click(function() {
				$('.portfolioFilter .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
				$container.isotope({

					filter: selector,
				});
				return false;
			});

		});
	</script>

</body>

</html>
