<?php
// Include config file
require_once "config.php";

$type = $brand = $size = $warr = $year  = $otr = "";
$type_err= "";

// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: index.php");
    exit;
}

if(isset($_POST["brand"]) && $_POST["size"] && $_POST["year"] || $_POST["warr"]) { 
        $param_type = isset($_POST["type"])? $_POST["type"]:'NA';
        $param_brand = $_POST["brand"];
        $param_size = $_POST["size"];
        $param_warr = $_POST["warr"];
        $param_year = $_POST["year"];
        $param_otr =  isset($_POST["other"])? $_POST["other"]:'NA';
        
    }

if($_SERVER["REQUEST_METHOD"] == "POST")
{

$sql = "UPDATE users SET brand = ?, otr_brand = ?, type = ?, size = ?, warranty = ?, year = ? WHERE id = ?";

  if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssiiii", $param_brand, $param_otr, $param_type, $param_size, $param_warr, $param_year, $param_id);
            
            // Set parameters
           
            $param_id = $_SESSION["id"];
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){

            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
	 mysqli_close($link);	
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <head><meta charset="utf-8">
	<meta name="viewport" content="width=device-width, maximum-scale=1">

	<title>Buy-Insurance</title>
	<link rel="icon" href="favicon.png" type="image/png">
	<link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='css/form.css' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    

</head>
<body>


<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">INSTAGADGET</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Home</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">

      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo htmlspecialchars($_SESSION["name"]); ?> <span class="glyphicon glyphicon-user"></span></a>
      <ul class="dropdown-menu">
          <li><a href="reset-password.php">Reset Password</a></li>
        </ul>
    </li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>

<div  style="padding-top:5%" class="wrap_cont">

   <div class="page-header">
        <h1>Thank you, <b><?php echo htmlspecialchars($_SESSION["name"]); ?></b>. Our executive will contact you soon.</h1>
    </div>

<div>
<h2>
TERMS AND CONDITIONS
</h2>
<!-- <p>
1. This contract is offered to the customers who are residing within the municipal limits of the city/ town of company's authorized service centers. However customers residing outside the municipal limits of the city / town of our service centre can also opt for this contract, but in this case service will be undertaken only at the service centre and the customer will have to bring the set at his own expense to the service centre.
<br>
2. In case the service contract is to be entered into after the expiry of warranty period or of previous contract, the contract will be accepted subject to checking of the equipment by the company's representative and verifying that it is in working condition. In case set is found defective and any repair needs to be done, then it will be repaired first and then taken into contract and the cost (labor and parts) shall be borne by the customer.
</p> -->
<br>
<div style="text-align:center;">
    <button onClick="window.location.href = 'tc.pdf';" id="t_c_btn" style="border: none;
    background: gray;
    color: white;
    border-radius: 50px;
    padding: 10px;
    padding-left: 20px;
    padding-right: 20px;">Read More</button>    
</div>

<!-- <div id="t_c" style="display:none">

<p>
3. All payments are to be made in advance along with the contract. 
<br>
4. The contract shall be valid for a period as mentioned in the contract form, or the Invoice, duly signed by INSTAGADGET SECURE representative. 

<br>5. Acceptance / Renewal of contract after expiry of the contract shall be at the discretion of the company. 

<br>6. The customer shall register the complaint at INSTAGADGET SECURE centers only. Any change of address must be notified by the customer in advance. 

<br>7. The company shall be under no obligation to provide repair / service because of improper use, unauthorized alteration, modification or substitution of any part or Sr. No. of the machine is altered, defaced or removed, abnormal voltage fluctuation, rat bite, neglect, acts of god like floods, lightening, earthquakes etc.. or causes other than ordinary use. If our services are required as a result of the causes stated above, such services shall be at extra charge. 

<br>8. All defective components shall be replaced with compatible working parts and defective parts shall be company's property. 

<br>9. While every effort shall be made to give preferential attention to emergency breakdown of the equipment, the company shall not be held responsible for any loss arising. 

<br>10. In case the customer wants to cancel the contract before the completion of the contract period, there shall be no refund of the charges for unexpired period. 

<br>11. The contract is not transferable in event of resale / gift to any other person and no refund shall be given. 

<br>12. The equipment brought to the service center will remain there at customer risk and the company will not be responsible for any damages caused due to the factors beyond its control. 

<br>13. Damage to the product or any parts due to Transportation / Shifting is not covered under this contract. 

<br>14. The AMC shall cover the following parts under the AMC contract. 

<br>15. HA/AC Products: Compressor, Thermostat, Relay, OLP, Fan Motors, Timers, PCB, Heaters, Air dampers, Ice maker, temperature sensors, Evaporators, Condensers, Refrigerant charging, Driers, Capillary, Blowers, Fan Blade, Control box, Magnetron, Electrical switches, transformers (internal), Diodes, Capacitors, Wire harness, Wash & Spin motors, Gear box, Clutch Assy., Drum, Inner tub, Bellows, bearing & lip seal, Drain motor, Pump, Electrical valves, Buzzer, Suspension, Key Membrane, Remote PCB.

<br>16. CE Products: Chassis, PCB, Tuner, Picture tube, LCD module, Electrical switches, PDP module, SMPS, FBT, Guns, Speakers, Capacitors, Diodes, Transformer, Transistors, IC, Remote PCB. 

<br>17. Main liner crack, Doors, Bulbs, rubber pads, remote, stabilizers, consumables, and any damage to the aesthetical components shall not be covered under this contract. 

<br>18. This contract does not include any kind of software support or installations. INSTAGADGET SECURE shall not be responsible for damage to, or loss of, any programs, data or removable storage media including any consequential loss or damage. 

<br>19. All third party peripherals, whether purchased as a part of a system, or bundled with it, come with the prevailing agent's Warranty, and INSTAGADGET SECURE makes no warranty whatsoever on their behalf. Example includes: Tata Indicom Phones, Printers or UPS. 

<br>20. This contract will not be valid for any item not supplied or certified from LGEIL and covers the base configuration only shipped from LGEIL Factory. Any Additions / Alterations to hardware will not form part of contract and will be chargeable to customer. 

<br>21. INSTAGADGET SECURE  will repair / replace any defective parts and correct any problems resulting from workmanship free of charge.  reserves the right to use reconditioned parts with performance parameters equal to those of new parts in connection with any services performed under  limited warranty. Defective parts needs to be submitted to INSTAGADGET SECURE 

<br>22. All disputes are subject to New Delhi Jurisdiction 

<br>23. Pricing, Terms and Conditions are subject to change without any notice 
 
</p>

</div> -->

</div>

<br><br>
<iframe src="plans.php" style="width: 100%;
    border: none;"></iframe>
<br><br>
<hr>
<br><br>
    <p>
        <p>Meanwhile, you can sign out of your account</p>
        <a href="logout.php" class="btn btn-danger">Click to Sign Out</a>
        <br>
        <br>
        <p>OR</p>
        <br>
        <p>Go to Account Home</p>
        <a href="welcome.php" class="btn btn-danger">Go to Home</a>
		 <div class="wrapper">
	
    </div>    
    </p>
</div>
    
    <script src="js/app.js">
    </script>

</body>
</html>